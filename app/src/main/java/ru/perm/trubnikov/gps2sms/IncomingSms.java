package ru.perm.trubnikov.gps2sms;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.telephony.SmsMessage;
import android.util.Log;

/**
 * Incoming SMS Handler here
 * *
 */

public class IncomingSms extends BroadcastReceiver {

    public static final int NOTIFICATION_ID = 1;
    String CHANNEL_ID = "GPS2SMS_CHANNEL";

    public void onReceive(Context context, Intent intent) {

        SharedPreferences sharedPrefs = PreferenceManager
                .getDefaultSharedPreferences(context);
        if (sharedPrefs.getBoolean("prefRegexpSMS", true)) {

            // Retrieves a map of extended data from the intent.
            final Bundle bundle = intent.getExtras();

            try {

                if (bundle != null) {

                    final Object[] pdusObj = (Object[]) bundle.get("pdus");

                    for (int i = 0; i < pdusObj.length; i++) {

                        SmsMessage currentMessage = SmsMessage
                                .createFromPdu((byte[]) pdusObj[i]);
                        // String phoneNumber =
                        // currentMessage.getDisplayOriginatingAddress();
                        // String senderNum = phoneNumber;
                        // Log.d("gps", "senderNum: "+ senderNum + "; message: "
                        // + message);

                        String Coordinates = GpsHelper.extractCoordinates(currentMessage.getDisplayMessageBody());

                        if (!Coordinates.equalsIgnoreCase("0,0")) {

                            sendNotification(context, Coordinates);
                            //DBHelper.openOnMap(context, Coordinates);

                        }

                        // Show Alert
                        // Toast toast = Toast.makeText(context, clip,
                        // Toast.LENGTH_LONG);
                        // toast.show();

                    } // end for loop
                } // bundle is null

            } catch (Exception e) {
                Log.d("gps", "Exception smsReceiver" + e);
            }

        }

    }

    private void createNotificationChannel(Context context) {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "GPS2SMS Channel";
            String description = "GPS2SMS Channel Description";
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = context.getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    public void sendNotification(Context context, String Coordinates) {

        Intent intent = GpsHelper.getIntentForMap(Coordinates);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        createNotificationChannel(context);

        NotificationCompat.Builder builder = new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_stat_notify_location)
                .setContentTitle(context.getResources().getString(R.string.sms_notify_title))
                .setContentText(context.getResources().getString(R.string.sms_notify_desc1))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher))
                // Set the intent that will fire when the user taps the notification
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(12345, builder.build());

    }


}